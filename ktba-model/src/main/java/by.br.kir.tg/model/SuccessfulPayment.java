package by.br.kir.tg.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by kiryl_c on 6/23/17.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class SuccessfulPayment {

    private String currency;
    private String total_amount;
    private String invoice_payload;
    private String shipping_option_id;
    private String order_info;
    private String telegram_payment_charge_id;
    private String provider_payment_charge_id;
}

package by.br.kir.tg.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by kiryl_c on 6/23/17.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Document {

    private String file_id;
    private PhotoSize thumb;
    private String file_name;
    private String mime_type;
    private Integer file_size;
}

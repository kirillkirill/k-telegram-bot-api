package by.br.kir.tg.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * Created by kiryl_c on 6/23/17.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class ReplyKeyboardMarkup implements Replyable {

    private List<KeyboardButton> keyboard;
    private Boolean resize_keyboard;
    private Boolean one_time_keyboard;
    private Boolean selective;
}

package by.br.kir.tg.rest;

import by.br.kir.tg.model.Message;
import by.br.kir.tg.model.Replyable;
import by.br.kir.tg.model.Result;
import by.br.kir.tg.model.Update;
import by.br.kir.tg.model.User;
import by.br.kir.tg.rest.model.SendMessageRequest;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.NonNull;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.fluent.ContentResponseHandler;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Created by kiryl_c on 6/23/17.
 */
public class TelegramBotRestApi {

    private static final Logger LOG = LoggerFactory.getLogger(TelegramBotRestApi.class);
    private static final String API_ENDPOINT = "https://api.telegram.org/bot%s/%s";
    private final String token;
    private ObjectMapper objectMapper;

    public TelegramBotRestApi(String token) {
        this.token = token;
        objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public Result<User> getMe() {
        String url = getUrl("getMe");
        Class<User> targetClazz = User.class;
        Iterable<NameValuePair> params = Collections.EMPTY_LIST;
        return callMethod(url, targetClazz, params);
    }

    private Result callMethod(String url, Class targetClazz, Iterable<NameValuePair> params) {
        LOG.debug("TelegramBotRestApi.callMethod {}", params);
        try {
            Response response = Request
                    .Post(url)
                    .bodyForm(params, Charset.forName("utf-8"))
                    .execute();
            HttpResponse httpResponse = response.returnResponse();
            String rawResponse = new ContentResponseHandler().handleEntity(httpResponse.getEntity()).asString();
            LOG.debug("rawResponse {}", rawResponse);
            JsonNode jsonNode = objectMapper.readTree(rawResponse);
            Result result = objectMapper.treeToValue(jsonNode, Result.class);
            if (jsonNode.has("ok") && jsonNode.get("ok").asBoolean()) {
                Object value = objectMapper.treeToValue(jsonNode.get("result"), targetClazz);
                result.setResult(value);
            }
            return result;
        } catch (IOException e) {
            e.printStackTrace();
            Result result = new Result();
            result.setDescription(e.toString());
            return result;
        }
    }

    public Result<Update[]> getUpdates(
        final Long offset,
        final Integer limit,
        final Integer timeout,
        final List<String> allowed_updates
    ) {
        String url = getUrl("getUpdates");
        Class targetClazz = Update[].class;
        List<NameValuePair> params = new ArrayList<>();
        addIfNotNull(params, "offset", offset);
        addIfNotNull(params, "limit", limit);
        addIfNotNull(params, "timeout", timeout);
        addIfNotNull(params, "allowed_updates", allowed_updates);
        return callMethod(url, targetClazz, params);
    }

    public Result<Message> sendMessage(SendMessageRequest request){
        return this.sendMessage(
            request.getChat_id(),
            request.getText(),
            request.getParse_mode(),
            request.getDisable_web_page_preview(),
            request.getDisable_notification(),
            request.getReply_to_message_id(),
            request.getReply_markup()
        );
    }

    public Result<Message> sendMessage(
        @NonNull
        final String chat_id,
        @NonNull
        final String text,
        final String parse_mode,
        final Boolean disable_web_page_preview,
        final Boolean disable_notification,
        final Long reply_to_message_id,
        final Replyable reply_markup
    ) {
        String url = getUrl("sendMessage");
        Class targetClazz = Message.class;
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("chat_id", chat_id));
        params.add(new BasicNameValuePair("text", text));

        addIfNotNull(params, "parse_mode", parse_mode);
        addIfNotNull(params, "disable_web_page_preview", disable_web_page_preview);
        addIfNotNull(params, "disable_notification", disable_notification);
        addIfNotNull(params, "reply_to_message_id", reply_to_message_id);
        addIfNotNull(params, "reply_markup", reply_markup);
        return callMethod(url, targetClazz, params);
    }

    public Result<Message> sendAudio(
        @NonNull
        final String chat_id,
        @NonNull
        final String audio,
        final String title,
        final String performer,
        final Boolean disable_notification,
        final Integer reply_to_message_id,
        final Replyable reply_markup
    ) {
        String url = getUrl("sendAudio");
        Class targetClazz = Message.class;
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("chat_id", chat_id));
        params.add(new BasicNameValuePair("audio", audio));

        addIfNotNull(params, "title", title);
        addIfNotNull(params, "performer", performer);
        addIfNotNull(params, "disable_notification", disable_notification);
        addIfNotNull(params, "reply_to_message_id", reply_to_message_id);
        addIfNotNull(params, "reply_markup", reply_markup);
        return callMethod(url, targetClazz, params);
    }

    public Result deleteMessage(
        @NonNull
        final String chat_id,
        @NonNull
        final Long message_id
    ) {
        String url = getUrl("deleteMessage");
        Class targetClazz = Boolean.class;
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("chat_id", chat_id));
        params.add(new BasicNameValuePair("message_id", String.valueOf(message_id)));
        return callMethod(url,targetClazz, params);
    }

    private void addIfNotNull(List<NameValuePair> params, String name, Object value) {
        Optional.ofNullable(value)
            .ifPresent(s -> params.add(new BasicNameValuePair(name, toJsonString(value))));
    }

    private String getUrl(String method) {
        return String.format(API_ENDPOINT, token, method);
    }

    private <T> T orElse(T src, T def) {
        return Optional.ofNullable(src).orElse(def);
    }

    private String toJsonString(Object object) {
        return objectMapper.valueToTree(object).toString();
    }
}

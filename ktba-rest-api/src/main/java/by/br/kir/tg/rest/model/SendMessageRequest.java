package by.br.kir.tg.rest.model;

import by.br.kir.tg.model.Replyable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Value;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SendMessageRequest {

    private String chat_id;
    private String text;
    private String parse_mode;
    private Boolean disable_web_page_preview;
    private Boolean disable_notification;
    private Long reply_to_message_id;
    private Replyable reply_markup;
}

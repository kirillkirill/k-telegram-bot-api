package by.br.kir.tg;

import by.br.kir.tg.model.InlineKeyboardButton;
import by.br.kir.tg.model.InlineKeyboardMarkup;
import by.br.kir.tg.model.Message;
import by.br.kir.tg.model.Result;
import by.br.kir.tg.model.Update;
import by.br.kir.tg.model.User;
import by.br.kir.tg.rest.TelegramBotRestApi;
import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.Arrays;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Created by kiryl_c on 6/23/17.
 */
@Ignore
public class HttpTest {

    @Test
    public void test1() throws IOException {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet("http://telegram.org/");
        CloseableHttpResponse response = null;
        response = httpclient.execute(httpGet);
        System.out.println(response.getStatusLine());
        HttpEntity entity1 = response.getEntity();
        response.close();

        Content content = Request.Get("http://telegram.org/")
            .execute().returnContent();
        System.out.println("content = " + content);
    }

    @Test
    public void test2() throws IOException {

        String
            r =
            String.format("https://api.telegram.org/bot%s/%s", URLEncoder.encode("123", "utf-8"),
                          URLEncoder.encode("get/Me", "utf-8"));
        System.out.println("r = " + r);
    }

    @Test
    public void test3() throws IOException {
        TelegramBotRestApi
            telegramBotRestApi =
            new TelegramBotRestApi(FileUtils.readFileToString(new File("/tmp/hashtestbot/token.txt"), "utf-8").trim());
        Result<User> me = telegramBotRestApi.getMe();
        System.out.println("me = " + me);
    }

    @Test
    public void test4() throws IOException {
        TelegramBotRestApi
            telegramBotRestApi =
            new TelegramBotRestApi(FileUtils.readFileToString(new File("/tmp/hashtestbot/token.txt"), "utf-8").trim());
        long t0 = System.currentTimeMillis();
        int n = 10;
        for (int i = 0; i < n; i++) {
            Result<Update[]> updates = telegramBotRestApi.getUpdates(0L, 100, null, null);
            System.out.println("updates = " + Arrays.toString(updates.getResult()));
        }
        long t1 = System.currentTimeMillis() - t0;
        double v = ((double) t1) / ((double) n);
        System.out.println("t1/n = " + v);
//        telegramBotRestApi.sendMessage("367627241","privet",null,null,null,null,null);
    }

    @Test
    public void test5() throws IOException {
        TelegramBotRestApi
            telegramBotRestApi =
            new TelegramBotRestApi(FileUtils.readFileToString(new File("/tmp/hashtestbot/token.txt"), "utf-8").trim());
        telegramBotRestApi.sendMessage("367627241","/a/b/c",null,null,null,null,null);
    }

    @Test
    public void test6() throws IOException {
        TelegramBotRestApi
            telegramBotRestApi =
            new TelegramBotRestApi(FileUtils.readFileToString(new File("/tmp/hashtestbot/token.txt"), "utf-8").trim());
        InlineKeyboardButton inlineKeyboardButton1 = new InlineKeyboardButton();
        inlineKeyboardButton1.setText("first");
        inlineKeyboardButton1.setCallback_data("first_data");
        InlineKeyboardButton inlineKeyboardButton2 = new InlineKeyboardButton();
        inlineKeyboardButton2.setText("second");
        inlineKeyboardButton2.setCallback_data("second_data");
        InlineKeyboardButton inlineKeyboardButton3 = new InlineKeyboardButton();
        inlineKeyboardButton3.setText("3rd");
        inlineKeyboardButton3.setCallback_data("3rd_data");

        Result<Message>
            choose =
            telegramBotRestApi.sendMessage("367627241", "Choose", null, null, null, null, new InlineKeyboardMarkup(
                Arrays.asList(
                    Arrays.asList(inlineKeyboardButton1, inlineKeyboardButton2),
                    Arrays.asList(inlineKeyboardButton3)
                )
            ));

        System.out.println("choose = " + choose);
        System.out.println("choose.getResult() = " + choose.getResult());
        System.out.println("choose.getDescription() = " + choose.getDescription());
    }

    @Test
    @Ignore
    public void test7() throws IOException {
        String token = "";
        TelegramBotRestApi telegramBotRestApi = new TelegramBotRestApi(token);
        String audio = "http://cdndl.zaycev.net/100001526/2472154/april_wine_-_april_wine_-_i_just_wanna_make_love_to_you_zaycev.net_%28zaycev.net%29.mp3";
        Result<Message> result = telegramBotRestApi
            .sendAudio("322245790", audio, "song", "band", true, null, null);
        System.out.println(result);
    }
}